// File: event.cpp

#include "event.h"
#include "kernel.h"
#include "kernelev.h"
#include "dos.h"

//#define SYSMOD_ON

//#include "sysmod.h"

Event::Event(IVTNo ivtNo) {
	LOCK;
	myImpl = new KernelEv(ivtNo);
	UNLOCK;
}

Event::~Event() {
	delete myImpl;
}

int Event::wait() {
	LOCK;
	int temp = myImpl->waitEv();
	UNLOCK;
	return temp;
}

void Event::signal() {
	LOCK;
		//SYSMOD_ENTER(0,0);
	myImpl->signalEv();
        //SYSMOD_EXIT(0,0);
	UNLOCK;
}

