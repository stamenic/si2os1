//File: eventlst.cpp

#include <string.h>
#include "eventlst.h"
#include "kernelev.h"


EventLista::EventLista(){
	first = NULL;
}

EventLista::~EventLista() {
	while (first) {
		Elem *temp = first;
		first = first->next;
		delete temp->kev;
		delete temp;
	}
}

//Ubacuje novi element u listu
void EventLista::insert(KernelEv* novi) {
	Elem *temp = new Elem (novi); 
	temp->next=first;
	first = temp;
}

//Vraca prvi Event u listi
KernelEv* EventLista::First(){
	if (first)
		return first->kev;
	else return NULL;
}

// Uzima prvi Event u listi
KernelEv* EventLista::getFirst() {
	if (first) {
		KernelEv* temp = first->kev;
		Elem* del = first;
		first = first->next;
		delete del;

		return temp;
	}
	else return NULL ;
}


//Uklanja Event iz liste
void EventLista::remove(KernelEv* del) {
	if (!first) return;


	for(Elem *prev = NULL, *i = first; i!=NULL; prev=i,i=i->next) {
		if (i->kev==del) {
			if (prev) prev->next=i->next;
			else first=i->next;

			delete i;
			return;
		}
	}
}


void EventLista::signalAllEvents(IVTNo ivtNo) {
	if (first) {	

		for(Elem *i = first; i!=NULL; i=i->next) {
			if (i->kev->ivtNo == ivtNo)	i->kev->signalEv();
		}
	}	
}
