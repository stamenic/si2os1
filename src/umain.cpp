// File: umain.cpp

#include "umain.h"

extern int userMain(int, char**);

UMain::UMain(int argc, char *argv[]) : Thread("uMain") {
	this->argc = argc;
	this->argv = argv;
	
	this->start();
}

void UMain::run() {
	returnValue = userMain(argc, argv);
}