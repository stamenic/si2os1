// File: ivtentry.cpp

#include "ivtentry.h"
#include <dos.h>
#include "kernelev.h"
#include "kernel.h"

IVTEntry::IVTEntry(IVTNo ivtNo, ISR eventRoutine) {
	LOCK;
	this->ivtNo = ivtNo;
	oldIntrR = getvect(ivtNo);
	setvect(ivtNo, eventRoutine);
	UNLOCK;
}

IVTEntry::~IVTEntry() {
	setvect(ivtNo, oldIntrR);
}

void IVTEntry::signalAll() {
	Kernel::blockEventList->signalAllEvents(ivtNo);
}