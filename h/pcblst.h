// File: pcblst.h

#ifndef _pcblst_h_
#define _pcblst_h_

#include <stdlib.h>
#include "pcb.h"

class PCB;

class PCBLista {

	struct Elem {
		PCB *pcb;
		Elem *next;

		Elem(PCB *p) {
			pcb = p;
			next = NULL;
		}
	} *first;

public:

	PCBLista();
	~PCBLista();

	void insert(PCB *);

	PCB* First();
	PCB* getFirst();

	void remove (PCB *);

	PCB* getPCBbyId(ID);
	ID getIdByName(TName);

	int isThereBlockedThread();
};

#endif