// File: kernelsm.h

#ifndef _kernelsm_h_
#define _kernelsm_h_

#include "pcblst.h"
#include "semaphor.h"

class PCBLista;

class KernelSem {
      private: PCBLista *blockedLista;

protected:
	int val;
	
	void block();
	void deblock();
	
public:
	KernelSem(int);
	~KernelSem();
	
	int wait();
	void signal();

	int Value();
	
	friend class PCB;
	friend class PCBLista;
};

#endif