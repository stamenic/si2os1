// File: kernel.h

#ifndef _kernel_h_
#define _kernel_h_

#include "pcblst.h"
#include "sleeplst.h"
#include "eventlst.h"
#include "thread.h"
#include "timer.h"
#include "idle.h"

#define LOCK asm{ pushf; cli }
#define UNLOCK asm{ popf; }

#define INTR 0x08


class Kernel {
public:
	static PCBLista *pcbList;
	static PCBLista *timedList;
	static PCB *running;
	static Timer *timer;
	static Thread *startingThread;
	static IdleThread *idleThread;
	
	static EventLista *blockEventList;
	static SleepLista* sleepLista;

	static ID ukId;

	static void initialization();
	static void deinitialization();
	
	static void interrupt dispatch(...);

};

#endif