// File: ivtentry.h

#ifndef _ivtentry_h_
#define _ivtentry_h_

typedef unsigned char IVTNo;

typedef void interrupt (*ISR)(...);//Interrupt Service Routine 

class IVTEntry {
	IVTNo ivtNo;
	
public:	
	IVTEntry(IVTNo, ISR);
	~IVTEntry();
	
	ISR oldIntrR;
	void signalAll();
};

#endif