// File:  eventlst.h

#ifndef _eventlst_h_
#define _eventlst_h_

#include <stdlib.h>
#include "kernelev.h"

class KernelEv;

class EventLista {

	struct Elem {
		KernelEv *kev;
		Elem *next;

		Elem(KernelEv *k) {
			kev = k;
			next = NULL;
		}
	} *first;

public:

	EventLista();
	~EventLista();

	void insert(KernelEv *);

	KernelEv* First();
	KernelEv* getFirst();

	void remove (KernelEv *);
	
	void signalAllEvents(IVTNo);
};

#endif