// File: umain.h

#ifndef _umain_h_
#define _umain_h_

#include "thread.h"

class UMain: public Thread {
	int argc;
	char **argv;
	
public:
	int returnValue;	
	UMain(int, char **);
	
	void run();
};

#endif