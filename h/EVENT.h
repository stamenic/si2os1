// File: event.h

#ifndef _event_h_
#define _event_h_

#include "ivtentry.h"


#define PREPAREENTRY(brUlaza,callOld)          \
	void interrupt eventRt##brUlaza(...);        \
	IVTEntry ivtEntry##brUlaza(brUlaza,eventRt##brUlaza);  \
	void interrupt eventRt##brUlaza(...){        \
		ivtEntry##brUlaza.signalAll();             \
		if (callOld) ivtEntry##brUlaza.oldIntrR(); \
	}

typedef unsigned char IVTNo;

class KernelEv; 

class Event { 
public: 
	Event (IVTNo ivtNo); 
	~Event (); 
	
	int wait (); 
	void signal(); 

private: 
	KernelEv* myImpl;
};

#endif