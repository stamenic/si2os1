// File: timer.h

#ifndef _timer_h_
#define _timer_h_

#include "thread.h"

typedef void interrupt (*ISR)(...);

class Timer {
	static ISR oldInterruptRoutine;
public:
	Timer();
	~Timer();
	
	static Time KernelTime;
	static Time ThreadTime;
	
	static void interrupt newInterruptRoutine(...);
};

#endif